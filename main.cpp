#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <set>
#include <algorithm>
using namespace std;

/*
 *
 *
 *26. Для вказаної директорії, ім’я якої передається при запуску
 *програми, виконати пошук та видалення (з підтвердженням) порожніх директорій.

 **/
//for open and create functions
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <dirent.h>
#include <unistd.h>



char **dir;
struct dirent *entry;
DIR *dp;


void regard(int a,char s[])
{ if (a!=0)
   {perror(s);
    exit(-1);
   }
};


// let  a folder won't  have
int is_folder(unsigned char d_type)
{ int i;
// чудеса!
//у папки d_type=4
  if (int(d_type)!=4)
   return 0;
  return 1;
}

int is_empty(const char *s) {


     DIR *ldir = opendir(s);
     if (ldir == NULL)
         return false;

   struct dirent *d;
    int n=0;
     while ( ((d = readdir(ldir)) != NULL )) {
         if(++n>2)
             break;
     }
    closedir(ldir);


    if ( n<= 2)
      // directory is empty
        return  true;
   return false;
}


void scan_and_make_list_for_folders(char *path, int head,int &tail, int recursively)
{
  vector <string> pathes;
  pathes.push_back(path);

 while(head<=tail)
 {  dp=opendir(pathes[head].c_str());
   if (dp==NULL)
     regard(errno,"opendir");

  while ((entry = readdir(dp)))
  {
    if ((strcmp(entry->d_name,".")) && (strcmp(entry->d_name,"..")) && (is_folder(entry->d_type)))
    {
       string path;
//      char path[glen];
      path =pathes[head];// избежать наклейки путей
      path+='/';
      path+=entry->d_name;

    if( recursively == false) {

     int count_source = count( pathes[0].begin(), pathes[0].end(),'/' );
     int count_candidate = count( path.begin(), path.end(), '/' );
//     cout<<count_source<<"  "<<count_candidate<<"\n";

     if ( count_candidate - count_source == 1  ) {
      pathes.push_back(path);
      tail++;
     }

    }
    else {
        pathes.push_back(path);
        tail++;
    }

  }
 }

   head++;
   closedir(dp);
  }

//   exit(0);
// for(int i=1; i< pathes.size(); i++)
//     cout<<pathes[i]<<"\n";

 for(int i=1; i< pathes.size(); i++)  {

     if (  !is_empty(pathes[i].c_str()) ) continue;


     cout<<pathes[i]<<"\n";

     int decision=0;
      cout<<"delete dir?  1 - yes, 0 - no ";
      cin>>decision;
      cout<<"\n";



      if ( decision == 1) {
          remove( pathes[i].c_str() );
      }

 }

}


int main(int argc, char *argv[])
{ //argc -is the number of strings in argv
  // argv - full string , include name of file
  int head=0,tail=0;

//  argc=2;
//  argv[2]="/media/sh/gekannt/test";
//проверка ключа, ввод параметров из строки, проверка на то директория или нет

// FOR REMOVAL
  //  global_path = argv[1];
//    global_path = "/media/sh/gekannt/test";

 int recursively =false;

  if( argc > 3 || argc<2)  // non recursively
  {
      cerr<<"wrong number parameters"<<endl;
      return -1;
  }


  if( argc == 3) {
      if ( strcmp( argv[2], "-r") == 0) {
          cout<<"recursive deletion\n";
          recursively = true;
      }
      else {
          cerr<<"unknown key  "<<argv[2]<<"\n";
          return -1;
      }
  }

  // check if it's a directory
  if (  (dp=opendir(argv[1])) == NULL  ) {
      cerr<<"it's not  a directory, can't be opened\n";
     return -1;
  }


   scan_and_make_list_for_folders(argv[1],head,tail, recursively);

  return 0;
}
